"""
custom exceptions to be used in fslgui

"""
class NotAValidKey(Exception):
    pass

class MissingRequiredKey(Exception):
    pass


