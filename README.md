# FSLGUI

The FSL GUI project contains all of the wxPython graphical user interfaces for FSL programs.

# For developers

create a python virtual environment

`cd /path/to/virtual/envs`

`python3 -m venv fslgui`

`source /path/to/virtual/envs/fslgui/bin/activate`

install the requirements

`pip3 install -r requirements.txt`

